<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<link rel="stylesheet" type="text/css" href="wp-content/themes/katie/css/main.css" />
<script defer src="https://use.fontawesome.com/releases/v5.0.4/js/all.js"></script>
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div id="wrapper" class="hfeed">
<header id="header" role="banner">
  <h1> I'm Katie. I code. </h1>
</header>
<div id="container">
