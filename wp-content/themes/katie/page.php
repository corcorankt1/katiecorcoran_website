<?php get_header(); ?>
<main>
  <section class="tools">
    <div class="current">
      <h2> CURRENT GIG </h2>
    </div>
    <div class="left-split">
      <h2>Gentleman's Box</h2>
      <h3 class="timeframe">August 2016 - Present</h3>
      <p>I started working with Gentleman's Box in August 2016. After splitting my time between client work at Element5 Digital and managing the Gentleman's Box website, I transitioned into a full-time role with Gentleman's Box in April 2017. From fixing bugs to implementing new functionality, it's always an interesting day at the office. As a small team, I have the opportunity to be involved in almost every aspect of the business. Heck, I even edit some content before it goes out!</p>
    </div>
    <div class="right-split">
      <h2>Do you want to learn more?</h2>
      <div class="find-more">
        <div id="linkedin">
          <p>Find me on LinkedIn</p>
          <a href="https://www.linkedin.com/in/katie-corcoran-70622829" target="_blank"><img src="wp-content/themes/katie/images/In-White-94px-TM.png"></a>
        </div>
        <div id="email">
          <p>Email me</p>
          <p id="email_address">katie@katiecorcoran.codes</p>
          <a href="mailto:katie@katiecorcoran.codes"><img src="wp-content/themes/katie/images/message.png"></a>
        </div>
        <div id="resume">
          <p>Download my resume</p>
          <a href="https://katiecorcoran.codes/wp-content/uploads/2018/01/Katie_Corcoran_Resume-2.pdf" target="_blank"><i class="fas fa-download"></i></a>
        </div>
      </div>
    </div>
    <div class="journey">
      <h2> JOURNEY TO GENTLEMAN'S BOX </h2>
    </div>
  </section>
  <section class="singing">
    <div class="left-split">
      <div class="white-box">
        <img src="wp-content/themes/katie/images/CMU_black.jpg">
      </div>
    </div>
    <div class="right-split">
      <h2>Singing at CMU</h2>
      <h3 class="timeframe"> September 2005 - May 2008</h3>
       <p>Before I had ever even considered a career in development, I was a Vocal Performance major at Central Michigan University. Even though I end up switching my major, the entire reason I went to CMU was for the music program. Without even knowing it, that decision set me on a path to become a developer because I met my future husband while I was there. He was studying Graphic Design.</p>
    </div>
  </section>
  <section class="pm">
    <div class="left-split">
      <h2>Construction at Rainbow</h2>
      <h3 class="timeframe"> September 2011 - April 2012</h3>
      <p>While I was attending online classes at CMU, I got a job working as a Construction Assistant at Rainbow Child Care Center. My boss, Rod, was amazing. He empowered me to take on as much as I wanted too. There was a maintenance software, ManagerPlus, for the facilities department that had been implemented a year prior that no one had touched since they purchased it. So, apart from my normal project management responsibilities, I took on the task of finishing the implementation of the software.</p>
    </div>
    <div class="right-split">
      <div class="white-box">
        <img src="wp-content/themes/katie/images/managerplus_logo.png">
      </div>
    </div>
  </section>
  <section class="salesforce">
    <div class="left-split">
      <div class="white-box">
        <img src="wp-content/themes/katie/images/Salesforce_Logo_RGB_1797c0_8_13_14.png">
      </div>
    </div>
    <div class="right-split">
      <h2>Salesforce at Lease Corp</h2>
      <h3 class="timeframe"> November 2012 - April 2016</h3>
      <p>I landed a job at Lease Corporation of America as an Executive Assitant. During my first month, I trained with the Salesforce Administrator and was catching on quickly. Sheer curiosity led me to dive deeper into the admin side of Salesforce. Two months after I started, the Salesforce Admin left the company. Until they found another admin, I took over. Over the next few months, I studied, took the exam, and became a Certifed Salesforce Administrator. Two years later I'm diving into building a custom application within Salesforce. My now husband and graphic designer, who I met all those years ago at CMU, says to me, &quot;It seems like the parts of the job that you like are the development parts. You should try web development.&quot;
      </p>
    </div>
  </section>
  <section class="bootcamp">
    <div class="left-split">
      <h2>Bootcamp at Grand Circus</h2>
      <h3 class="timeframe"> March 2016 - May 2016</h3>
      <p>Thanks to my husband, I started teaching myself web development. I started with CodeAcademy and FreeCodeCamp but was left wanting more. I started doing research on how I could get some more hands-on training when I came across Grand Circus - Detroit. They had just started a Part-time Front-end bootcamp, which was great because I had a full-time job. After talking about it at length, my husband and I decided that I should dive in and enroll.  Twelve weeks of classes and a lot of long nights led me to finish the bootcamp in May 2016. That led to an internship at Deep Canvas and before I knew it, I was a developer at Element5 Digital and Gentleman's Box.</p>
    </div>
    <div class="right-split">
      <div class="white-box">
        <img src="wp-content/themes/katie/images/web-rgb_dark-4-300x188.png">
      </div>
    </div>
  </section>
  <section class="contact">
    <a href="mailto:katie@katiecorcoran.codes">Email</a>
  </section>
</main>
<?php get_footer(); ?>
