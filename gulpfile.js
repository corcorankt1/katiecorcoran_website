var gulp = require('gulp');
var autoprefixer = require('gulp-autoprefixer');
var cleanCSS = require('gulp-clean-css')
var sass = require('gulp-ruby-sass');
var babel = require("gulp-babel");
var uglify = require('gulp-uglify');
var pump = require('pump');

gulp.task("babel", function () {
  return gulp.src("wp-content/themes/katie/js/main.js")
    .pipe(babel())
    .pipe(gulp.dest("wp-content/themes/katie/js/babel"));
});
gulp.task('compress', function (cb) {
  pump([
        gulp.src('wp-content/themes/katie/js/babel/*.js'),
        uglify(),
        gulp.dest('wp-content/themes/katie/js/output')
    ],
    cb
  );
});
gulp.task('sass', () =>
    sass('wp-content/themes/katie/css/main.scss')
        .on('error', sass.logError)
        .pipe(gulp.dest('wp-content/themes/katie/css'))
);
gulp.task('autoprefixer',function(){
  gulp.src('wp-content/themes/katie/css/main.css')
    .pipe(autoprefixer())
    .pipe(gulp.dest('wp-content/themes/katie/css/autoprefix'))
});
gulp.task('cleanCSS',function(){
  gulp.src('wp-content/themes/katie/css/main.css')
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('wp-content/themes/katie/css/minified'))
});
gulp.task('outputCSS',function(){
    gulp.src('wp-content/themes/katie/css/main.css')
    .pipe(autoprefixer())
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('wp-content/themes/katie/css/output.css'))
});
